 package unquote

 import "strings"

 func UnquoteString(text string) string {
    unquoteRunes := []rune(strings.TrimSpace(text))

    if unquoteRunes[0] == '"' && unquoteRunes[len(unquoteRunes)-1] == '"' {
        unquoteRunes = unquoteRunes[1:len(unquoteRunes)-1]
    }
    return string(unquoteRunes)
 }
