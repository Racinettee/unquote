package main

import (
    "fmt"
    "io/ioutil"
    "os"
    flags "github.com/jessevdk/go-flags"
    "gitlab.com/Racinettee/unquote/lib"
)

func main() {
     var opts struct {
        Quote string `short:"q" long:"quote" description:"The string to unquote, if not supplied stdin is used" option:"true"`
    }
    _, err := flags.Parse(&opts)
    if err != nil {
        return
    }

    unquoteText := opts.Quote
    if unquoteText == "" {
        bytes, err := ioutil.ReadAll(os.Stdin)

        if err != nil {
            os.Exit(-1)
        }
        unquoteText = string(bytes)
    }

    unquoteText = unquote.UnquoteString(unquoteText)
    fmt.Print(unquoteText)
}
